﻿using Deans_Office.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Deans_Office
{
    /// <summary>
    /// Logica di interazione per DialogBox.xaml
    /// </summary>
    public partial class DialogBox : Window
    {

        Student student = null;
        MainWindow win;
        public DialogBox(MainWindow mainWindow)
        {
            InitializeComponent();

            win = mainWindow;

            Studia.ItemsSource = StudentsDbService.GetStudia();
            Przedmioty.ItemsSource = StudentsDbService.GetSubjects();


        }

        /// <summary>
        /// Construct the Dialog window for the student data editing 
        /// </summary>
        /// <param name="stud"> Student class object </param>
        public DialogBox(Student stud, MainWindow mainWindow)
        {
            InitializeComponent();

            win = mainWindow;

            Studia.ItemsSource = StudentsDbService.GetStudia();
            Przedmioty.ItemsSource = StudentsDbService.GetSubjects();

            student = stud;
            LoadData(student);

        }

        /// <summary>
        /// Loads the students data into the form fields
        /// </summary>
        /// <param name="stud"> Student class object </param>
        private void LoadData(Student stud)
        {
            FName.Text = stud.FirstName;
            LastName.Text = stud.LastName;
            Address.Text = stud.Address;
            IndexNumber.Text = stud.IndexNumber;

            //Loads the studies
            foreach (string str in Studia.ItemsSource)
            {
                if (str.ToString().Equals(stud.Studia))
                {
                    Studia.SelectedItem = str;
                }
            }

            //Loads the subjects
            if (stud.Przedmioty != null)
            { 
                foreach (string str in stud.Przedmioty)
                {
                    Przedmioty.SelectedItems.Add(str);

                }
            }

        }

        /// <summary>
        /// Send A student class object to the MainIwndow to save his data
        /// then close the dialog box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            List<string> str = new List<string>();
            foreach(string itm in Przedmioty.SelectedItems)
            {
                str.Add(itm);
            }

            if (student == null)
            {
                student = new Student { FirstName = FName.Text, LastName = LastName.Text, IndexNumber = IndexNumber.Text, Address = Address.Text, Studia = Studia.SelectedItem.ToString()};
                student.Przedmioty = str;

            }
            else
            {
                student.FirstName = FName.Text;
                student.LastName = LastName.Text;
                student.IndexNumber = IndexNumber.Text;
                student.Address = Address.Text;
                student.Studia = Studia.SelectedItem.ToString();
                student.Przedmioty = str;
            }

            win.SaveData(student);
            Console.WriteLine(student);

            Close();

        }

        /// <summary>
        /// Close the dialog box canceling the action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
