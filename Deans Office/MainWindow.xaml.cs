﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Deans_Office.DAL;

namespace Deans_Office
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static ObservableCollection<Student> studList;
        static StudentsDbService DB = new StudentsDbService();
        public MainWindow()
        {
            InitializeComponent();

            studList = DB.WypełniajDany();
            StudentsData.ItemsSource = studList;
        }

        /// <summary>
        /// Writes the current selection size from the datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StudentsData_Selected(object sender, RoutedEventArgs e)
        {
            LabelX.Content = "You have selected " + StudentsData.SelectedItems.Count + " items";
        }

        /// <summary>
        /// Run the dialogbox for adding a new record into the data source
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dodaj_Click(object sender, RoutedEventArgs e)
        {
            var window = new DialogBox(this);
            window.Show();
        }

        /// <summary>
        /// Runs the action to delete data from the data source
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Usun_Click(object sender, RoutedEventArgs e)
        {

            Student[] lista = new Student[StudentsData.SelectedItems.Count];
            StudentsData.SelectedItems.CopyTo(lista,0);
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                DB.DeleteData(lista);
            }

            studList = DB.WypełniajDany();
            StudentsData.ItemsSource = studList;

        }

        /// <summary>
        /// Save a new record into the datasource or update an existing one
        /// </summary>
        /// <param name="stud"></param>
        public void SaveData(Student stud)
        {
            DB.SaveData(stud);

            studList = DB.WypełniajDany();
            StudentsData.ItemsSource = studList;
        }

        /// <summary>
        /// Starts action for editing the selected student
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StudentsData_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Student stud = (Student)StudentsData.SelectedItem;

            var window = new DialogBox(stud, this);
            window.Show();
        }
    }
}
