﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deans_Office.DAL
{
    public class Student
    {
        public int IdStudent {get; set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IndexNumber { get; set; }
        public string Address { get; set; }
        public string Studia { get; set; }
        public List<string> Przedmioty = new List<string>();

    }
}
