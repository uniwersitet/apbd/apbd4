﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace Deans_Office.DAL
{
    class StudentsDbService
    {
        /// <summary>
        /// Loads the data into the Data Grid on main window
        /// </summary>
        /// <returns> List of Students objects </returns>
        public ObservableCollection<Student> WypełniajDany()
        {
            ObservableCollection<Student> studList = new ObservableCollection<Student>();

            string connectionString = "Data Source=LAPTOP-4VV83EOT\\SQLEXPRESS;Initial Catalog=master;Integrated Security=True";

            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();

                using (SqlCommand command = new SqlCommand("SELECT a.IdStudent, a.FirstName, a.LastName, a.Address, a.IndexNumber, b.Name " +
                                                            "FROM apbd.Student a " +
                                                            "JOIN apbd.Studies b ON b.IdStudies = a.IdStudies", con))

                //Loading the students data into the grid
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        studList.Add(new Student
                        {
                            IdStudent = int.Parse(reader["IdStudent"].ToString()),
                            FirstName = reader["FirstName"].ToString(),
                            LastName = reader["LastName"].ToString(),
                            IndexNumber = reader["IndexNumber"].ToString(),
                            Address = reader["Address"].ToString(),
                            Studia = reader["Name"].ToString()
                        });
                    }
                }

                //Getting the subjects data of the students
                foreach (Student stud in studList)
                {
                    using (SqlCommand command2 = new SqlCommand("SELECT a.Name, b.Idstudent FROM apbd.Subject a, apbd.Student_Subject b " +
                                                                "WHERE b.IdSubject = a.IdSubject AND b.IdStudent = @Id", con))
                    {
                        command2.Parameters.AddWithValue("@Id", stud.IdStudent);
                        
                        using (SqlDataReader reader = command2.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                stud.Przedmioty.Add(reader["Name"].ToString());
                            }
                        }
                    }   
                }
            }

            return studList;
        }

        /// <summary>
        /// Loads all the necessary data for the ComboBox in the dialog window
        /// from the Studies table
        /// </summary>
        /// <returns> List of strings </returns>
        internal static IEnumerable GetStudia()
        {
            List<string> Studies = new List<string>();

            string connectionString = "Data Source=LAPTOP-4VV83EOT\\SQLEXPRESS;Initial Catalog=master;Integrated Security=True";

            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();

                using (SqlCommand command = new SqlCommand("SELECT Name FROM apbd.Studies", con))
                
                //Loading the studies data
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Studies.Add(reader["Name"].ToString());
                    }
                }
            }

            return Studies;
        }

        /// <summary>
        /// Loads all the necessary data for the ListBox control n the dialog window
        /// from the Subject table
        /// </summary>
        /// <returns> List of strings </returns>
        internal static IEnumerable<string> GetSubjects()
        {
            List<string> Subjects = new List<string>();

            string connectionString = "Data Source=LAPTOP-4VV83EOT\\SQLEXPRESS;Initial Catalog=master;Integrated Security=True";

            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();

                using (SqlCommand command = new SqlCommand("SELECT Name FROM apbd.Subject", con))
                
                //Loading the subjects data
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Subjects.Add(reader["Name"].ToString());
                    }
                }
            }

            return Subjects;
        }

        /// <summary>
        /// Saves data into the database
        /// (Tables Student and Student_Subject)
        /// </summary>
        /// <param name="stud"> Student class object </param>
        internal void SaveData(Student stud)
        {
            string connectionString = "Data Source=LAPTOP-4VV83EOT\\SQLEXPRESS;Initial Catalog=master;Integrated Security=True";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                var tran = con.BeginTransaction();

                using (SqlCommand command = new SqlCommand("AddStudent", con, tran))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", stud.IdStudent);
                    command.Parameters.AddWithValue("@firstname", stud.FirstName);
                    command.Parameters.AddWithValue("@lastname", stud.LastName);
                    command.Parameters.AddWithValue("@address", stud.Address);
                    command.Parameters.AddWithValue("@indexnumber", stud.IndexNumber);
                    command.Parameters.AddWithValue("@studies", stud.Studia);
                    command.ExecuteNonQuery();
                }


                using (SqlCommand command = new SqlCommand("DELETE FROM apbd.Student_Subject WHERE IdStudent=@Id", con, tran))
                {
                    command.Parameters.AddWithValue("@Id", stud.IdStudent);
                    int affectedRows = command.ExecuteNonQuery();
                }


                foreach (string str in stud.Przedmioty)
                {
                    using (SqlCommand command2 = new SqlCommand("UpdateStudentSubjects", con, tran))
                    {
                        command2.CommandType = CommandType.StoredProcedure;
                        command2.Parameters.AddWithValue("@student", stud.IdStudent);
                        command2.Parameters.AddWithValue("@subject", str);
                        command2.ExecuteNonQuery();
                    }
                }

                tran.Commit();
            }
        }

        /// <summary>
        /// Deletes the given students records from the data base
        /// (Tables Student and Student_Subject)
        /// </summary>
        /// <param name="lista">Student class object list</param>
        internal void DeleteData(Student[] lista)
        {
            string connectionString = "Data Source=LAPTOP-4VV83EOT\\SQLEXPRESS;Initial Catalog=master;Integrated Security=True";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                var tran = con.BeginTransaction();
                foreach (Student stud in lista)
                {
                    //Deleting data from Student_Subject table
                    using (SqlCommand command = new SqlCommand("DELETE FROM apbd.Student_Subject WHERE IdStudent=@Id", con, tran))
                    {
                        command.Parameters.AddWithValue("@Id", stud.IdStudent);
                        int affectedRows = command.ExecuteNonQuery();
                    }

                    //Deleting data from Student table
                    using (SqlCommand command2 = new SqlCommand("DELETE FROM apbd.Student WHERE IdStudent=@Id", con, tran))
                    {
                        command2.Parameters.AddWithValue("@Id", stud.IdStudent);
                        int affectedRows = command2.ExecuteNonQuery();
                    }
                }
                tran.Commit();
            }
        }
    }
}
