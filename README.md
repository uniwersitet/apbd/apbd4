**Ćwiczenia 5**

W niniejszych ćwiczeniach zajmiemy się komunikacją z bazą danych. W tych
ćwiczeniach połączycie wszystkie poprzednie umiejętności w celu stworzenia w pełni
funkcjonalnej aplikacji, która będzie się komunikować z bazą danych.
W załączniku do zadania znajdują się przykłady kodu prezentujące komunikację z bazą
danych.
Co powinieneś wiedzieć po ćwiczeniach?
* Jak komunikować się z bazą danych za pomocą klas SqlConnection i SqlCommand?
* Jak pobierać dane z bazy danych (SELECT)?
* Jak zapisywać dane w bazie danych (INSERT)?
* Jak aktualizować dane w bazie danych (UPDATE)?
* Jak uruchamiać procedurę składowaną?
* Jak wykorzystać mechanizm transakcji?

**Zadanie 1**

W pierwszym zadaniu należy przygotować swoją bazę danych. W załączniku
znajdziecie plik apbd_db_create.sql, który zawiera skrypt tworzący bazę danych. Baza
zawiera 4 tabele. Jej schemat znajduje się w załączniku do zadania.
Bazę proszę stworzyć na serwerze db-mssql.



**ekran listy studentów**

Tworzycie nową aplikację WPF o nazwie DeansOffice. Aplikacja będzie zarządzała danymi studentów, studiami i przedmiotami jakie dany student wybrał. 
W pierwszym zadaniu przygotujcie pierwsze okno zawierające kontrolkę DataGrid, przyciski i kontrolkę
Label. Poniżej znajduje się kilka uwag ogólnych.
1. W momencie uruchomienia chcemy od razu pobierać dane z bazy danych na temat studentów i je wyświetlać.
2. Komunikacja z bazą danych powinna być wydzielona do osobnego pliku. W tym wypadku najlepiej stworzyć osobny folder DAL (ang. Data Access Layer) i tam umieścić klasę StudentsDbService.cs. Klasa powinna enkapsulować całę logikę związaną z dostępem do bazy danych.
3. Dbamy o nazwy zmiennych i formatowanie kodu.
4. Dbamy o to, aby layout zachowywał się poprawnie podczas zmiany okna.
5. Nie chcemy, aby użytkownik miał możliwość edycji danych bezpośrednio poprzez kontrolkę DataGrid.
6. Dane w tabeli wymagają łączenia informacji pochodzących z wielu tabel.
7. Nie chcemy wyświetlać użytkownikowi wartości kluczy głównych.
8. Wszystkie kolumny w kontrolce DataGrid powinny mieć poprawne gramatycznie nagłówki.
9. Pamiętajmy, aby zawsze odpowiednio oprogramować błędy - szczególnie komunikację z bazą danych, która zawsze może zakończyć się błędem.
10. Staramy się wydzielić powtarzające się fragmenty kodu zgodnie z zasadami KISS i DRY.
11. Dbamy o to, aby aplikacja w żadnym wypadku nie mogła się niespodziewanie zamknąć. Testujemy aplikację w momencie różnych zachowań użytkownika.

**Zadanie 2**

1. Dodaj możliwość usuwania wybranych studentów z listy.
2. Użytkownik powinien mieć możliwość wyboru wielu studentów z kontrolki DataGrid.
3. W momencie wybrania x wierszy - pod kontrolką DataGrid powinien wyświetlać się automatycznie status aktualnego wyboru. Np. “Wybrałeś 5 wierszy”.
4. Po kliknięciu przycisku “Usuń” - użytkownik powinien otrzymać okno dialogowe w którym musi potwierdzić swój wybór - “Czy na pewno chcesz usunąć dane studentów?”.
5. Po zatwierdzenie usuwamy wszystkich studentów z bazy danych. Pamiętaj, że musimy usuwać dane zgodnie z kolejności referencji kluczy obcych, żeby zachować integralność danych. Kontrolki DataGrid powinna automatycznie się odświeżyć i usunąć wybranych studentów z listy.
6. Pamiętaj: chcemy usunąć wszystkich wybranych studentów, albo żadnego. Można tutaj wykorzystać procedurę składowaną lub mechanizm transakcji. Chcemy zabezpieczyć się przed taką sytuację, że np. Użytkownik wybrał 5 studentów - 3 zostało usuniętych, a dwóch nie.

**Zadanie 3**

1. Dodaj możliwość dodawania nowego studenta.
2. Po kliknięciu przycisku “Dodaj”, chcemy pokazać okno dialogowe.
3. Pamiętaj, że okno dialogowa służące do dodawania lub edycji nie powinno zawierać logiki związanej z komunikacją z bazą danych. Okno dialogowe służy wyłącznie do konstruowania interesującego nas obiektu.
4. Okno dialogowe po kliknięciu “Ok” powinno zwrócić nowy obiekt do okna głównego. Następnie zapis powinien odbywać się w oknie głównym. Dzięki temu mamy większą szansę na wykorzystanie okna dialogowego w różnych miejscach w naszej aplikacji w miarę jak będzie się rozrastać.
5. Okno dialogowe nie powinno mieć bezpośredniej referencji do okna głównego.
6. Okno dialogowe powinna wyświetlać w kontrolce ListBox listę dostępnych do wyboru przedmiotów. Użytkownik może zaznaczyć dowolnie dużo przedmiotów. Następnie zostaną one automatycznie przypisane do danego studenta.
7. Numer indeksu studenta powinien mieć format “s1234” - litera “s” i dowolna liczba cyfr.

**Zadanie 4**

1. Dodajemy możliwość edycji studenta. Najlepiej skorzystać z tego samego okna, które służy do dodawania nowego studenta.
2. Obiekt student powinien być przekazany do nowego okna.
3. W trakcie edycji w oknie powinny się wyświetlić aktualne dane studenta, które możemy zmodyfikować.
4. Po zapisaniu zmian, chcemy, aby dane studenta w kontrolce DataGrid uległy odświeżeniu.
5. Pamiętamy cały czas o walidacji i kontroli błędów.