﻿CREATE OR ALTER PROCEDURE UpdateStudentSubjects @student varchar(100), @subject varchar(100)
AS
	BEGIN
		DECLARE @Idsub int
		DECLARE @IdStud int
		DECLARE @datenow datetime
		DECLARE @count int

		SET @IdStud = (SELECT IdStudent FROM apbd.Student WHERE FirstName = @student)
		SET @Idsub = (SELECT IdSubject FROM apbd.Subject WHERE Name = @subject)
		IF (@IdStud NOT IN (SELECT IdStudent FROM apbd.Student_Subject))
			BEGIN
				SET @datenow = SYSDATETIME()
				SET @count = (SELECT COUNT(*) + 1 FROM apbd.Student_Subject) 

				INSERT INTO apbd.Student_Subject (IdStudentSubject, IdStudent, IdSubject, CreatedAt)
				values(@count, @IdStud, @Idsub, @datenow)
			END
		ELSE
			BEGIN
				IF (@Idsub NOT IN (SELECT IdSubject FROM apbd.Student_Subject WHERE IdStudent = @Student))
					BEGIN
						SET @datenow = SYSDATETIME()
						SET @count = (SELECT COUNT(*) + 1 FROM apbd.Student_Subject) 

						INSERT INTO apbd.Student_Subject (IdStudentSubject, IdStudent, IdSubject, CreatedAt)
						values(@count, @IdStud, @Idsub, @datenow)
					END
			END
	END