﻿CREATE OR ALTER PROCEDURE UpdateStudentSubjects @student int, @subject varchar(100)
AS
	BEGIN
		DECLARE @Idsub int
		DECLARE @IdStud int
		DECLARE @datenow datetime

		SET @IdStud = @student
		SET @Idsub = (SELECT IdSubject FROM apbd.Subject WHERE Name = @subject)
		IF (@IdStud NOT IN (SELECT IdStudent FROM apbd.Student_Subject))
			BEGIN
				SET @datenow = SYSDATETIME()

				INSERT INTO apbd.Student_Subject (IdStudent, IdSubject, CreatedAt)
				values(@IdStud, @Idsub, @datenow)
			END
		ELSE
			BEGIN
				IF (@Idsub NOT IN (SELECT IdSubject FROM apbd.Student_Subject WHERE IdStudent = @Student))
					BEGIN
						SET @datenow = SYSDATETIME()
						
						INSERT INTO apbd.Student_Subject (IdStudent, IdSubject, CreatedAt)
						values(@IdStud, @Idsub, @datenow)
					END
			END
	END